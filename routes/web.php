<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Auth\LoginController@login')->name("login");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('profesores','ProfesorController');
Route::resource('grados','GradoController');
Route::resource('alumnos','AlumnoController');
Route::resource('bloques','BloqueController');
Route::resource('calificaciones','CalificacionController');
Route::post('calificaciones/delete','CalificacionController@delete')->name('deleteCalificaciones');


