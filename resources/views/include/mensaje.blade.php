@if(Session::has('info') || Session::has('typealert'))
<div class="container">
  <div class="shadow alert alert-{{Session::get('typealert')}}" style="display:none;">
    {{ Session::get('info') }}
  </div>

</div>
@endif
@if($errors->any())
<div class="container">
  <div class="shadow alert alert-danger" style="display:none;">
    @if($errors->any())
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
    @endif
  </div>
</div>
@endif



<script>
  $('.alert').slideDown();
	setTimeout(()=> $('.alert').slideUp(),10000)
</script>
