@extends('layouts.master')
@section('title','Grados')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h1 class="text-center text-primary">Agregar Grado</h1>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ route('grados.store') }}">
            {{ csrf_field() }}
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" name="nombre" id="nombre" value="{{ old('nombre') }}">
              </div>

              <div class="form-group col-md-6">
                <label for="foto">Profesor:</label>
                <select name="profesor_id" id="profesor_id" class="form-control">
                  @empty(!$profesores)
                  @foreach ($profesores as $p)
                  <option value="{{$p->id}}">{{$p->nombre_completo}}</option>
                  @endforeach
                  @endempty

                </select>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="foto">Estatus:</label>
                <select name="estatus" id="estatus" class="form-control">
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Crear Grado</button>
            <a href="{{ route('grados.index') }}" class="btn btn-link">Regresar al listado de
              grados</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
