@extends('layouts.master')
@section('title','Grados')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h1 class="text-center text-primary">Listado de Grados</h1>
          Grados |
          Página
          {{$grados->currentPage()}}
          de
          {{$grados->lastPage()}}
        </div>
        <div class="card-body">
          <h1 class="text-center text-primary"></h1>
          <a class="btn btn-primary my-2" href="{{route('grados.create')}}">Agregar Grado<span
              class="mdi mdi-plus"></span>
          </a>
          <table class="table table-hover table-striped table-bordered table-sm">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre Completo</th>
                <th scope="col">Profesor</th>
                <th scope="col">Estatus</th>
                <th scope="col" colspan="2" class="text-center">Opciones</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($grados as $gra)
              <tr class="text-center">
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$gra->nombre}}</td>
                <td>{{$gra->profesor->nombre_completo}}</td>
                <td>{{($gra->estatus == 1)?'Activo':'Inactivo'}}</td>
                <td>
                  <a class="btn btn-success mdi mdi-pencil-outline" href="{{route('grados.edit',$gra->id)}}">Editar</a>
                </td>

              </tr>
              @empty
              <h3 class="bg-danger text-white text-center">No hay datos</h3>
              @endforelse

            </tbody>

          </table>
          {{ $grados->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
