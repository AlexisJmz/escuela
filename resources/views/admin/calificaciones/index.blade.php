@extends('layouts.master')
@section('title','Calificaciones')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h1 class="text-center text-primary">Listado de calificaciones</h1>
          calificaciones |
          Página
          {{$calificaciones->currentPage()}}
          de
          {{$calificaciones->lastPage()}}
        </div>
        <div class="card-body">
          <h1 class="text-center text-primary"></h1>
          <a class="btn btn-primary my-2" href="{{route('calificaciones.create')}}">Agregar Calificacion a alumno
          </a>
          <form action="{{route('deleteCalificaciones')}}" method="POST">
            {{ csrf_field() }}
            <button class="btn btn-danger my-2" type="submit">Eliminar Calificaciones<span
              class="mdi mdi-plus"></span></button>
          </form>

          <table class="table table-hover table-striped table-bordered table-sm">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Calificacion</th>
                <th scope="col">Alumno</th>
                <th scope="col">Bloque</th>
                <th scope="col" colspan="2" class="text-center">Opciones</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($calificaciones as $cal)
              <tr class="text-center">
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$cal->calificacion}}</td>
                <td>{{$cal->alumno->nombre_completo}}</td>
                <td>{{$cal->bloque->nombre}}</td>
                <td>
                  <a class="btn btn-success mdi mdi-pencil-outline" href="{{route('calificaciones.edit',$cal->id)}}">Editar</a>
                </td>

              </tr>
              @empty
              <h3 class="bg-danger text-white text-center">No hay datos</h3>
              @endforelse

            </tbody>

          </table>
          {{ $calificaciones->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
