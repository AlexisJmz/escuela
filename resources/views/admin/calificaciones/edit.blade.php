@extends('layouts.master')
@section('title','Calificaciones')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h1 class="text-center text-primary">Editar Calificacion</h1>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ route('calificaciones.update',$calificacion->id) }}">
            {{ method_field('PUT')}}
            {{ csrf_field() }}
            <div class="row">
              <div class="form-group col-md-6">
                <label for="foto">Alumno:</label>
                <select name="alumno_id" id="alumno_id" class="form-control">
                  @empty(!$alumnos)
                  @foreach ($alumnos as $a)

                  <option value="{{$a->id}}" {{($calificacion->alumno_id == $a->id)?'selected':''}}>{{$a->nombre_completo}}
                  </option>

                  @endforeach
                  @endempty

                </select>
              </div>

              <div class="form-group col-md-6">
                <label for="foto">Bloque:</label>
                <select name="bloque_id" id="bloque_id" class="form-control">
                  @empty(!$bloques)
                  @foreach ($bloques as $b)

                  <option value="{{$b->id}}" {{($calificacion->bloque_id == $b->id)?'selected':''}}>{{$b->nombre}}
                  </option>

                  @endforeach
                  @endempty

                </select>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="calificacion">Calificacion</label>
                <input type="text" class="form-control" name="calificacion" id="calificacion" value="{{ old('calificacion',$calificacion->calificacion) }}">
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Editar Calificacion</button>
            <a href="{{ route('calificaciones.index') }}" class="btn btn-link">Regresar al listado de
              calificaciones</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
