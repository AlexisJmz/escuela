@extends('layouts.master')
@section('title','bloques')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h1 class="text-center text-primary">Editar Bloque</h1>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ route('bloques.update',$bloque->id) }}">
            {{ method_field('PUT')}}
            {{ csrf_field() }}
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" name="nombre" id="nombre"
                  value="{{ old('nombre',$bloque->nombre) }}">
              </div>

              <div class="form-group col-md-6">
                <label for="grado_id">Grado:</label>
                <select name="grado_id" id="grado_id" class="form-control">
                  @empty(!$grados)
                  @foreach ($grados as $g)

                  <option value="{{$g->id}}" {{($bloque->grado_id == $g->id)?'selected':''}}>{{$g->nombre}}
                  </option>

                  @endforeach
                  @endempty

                </select>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="">Estatus:</label>
                <select name="estatus" id="estatus" class="form-control">
                  @if($bloque->estatus == 1)
                  <option value="1" selected>Activo</option>
                  <option value="0">Inactivo</option>
                  @else
                  <option value="1">Activo</option>
                  <option value="0" selected>Inactivo</option>
                  @endif
                </select>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Editar Bloque</button>
            <a href="{{ route('bloques.index') }}" class="btn btn-link">Regresar al listado de
              bloques</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
