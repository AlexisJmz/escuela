@extends('layouts.master')
@section('title','Bloques')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h1 class="text-center text-primary">Listado de bloques</h1>
          bloques |
          Página
          {{$bloques->currentPage()}}
          de
          {{$bloques->lastPage()}}
        </div>
        <div class="card-body">
          <h1 class="text-center text-primary"></h1>
          <a class="btn btn-primary my-2" href="{{route('bloques.create')}}">Agregar Bloque<span
              class="mdi mdi-plus"></span>
          </a>
          <table class="table table-hover table-striped table-bordered table-sm">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre Completo</th>
                <th scope="col">Grado</th>
                <th scope="col">Estatus</th>
                <th scope="col" colspan="2" class="text-center">Opciones</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($bloques as $blo)
              <tr class="text-center">
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$blo->nombre}}</td>
                <td>{{$blo->grado->nombre}}</td>
                <td>{{($blo->estatus == 1)?'Activo':'Inactivo'}}</td>
                <td>
                  <a class="btn btn-success mdi mdi-pencil-outline" href="{{route('bloques.edit',$blo->id)}}">Editar</a>
                </td>

              </tr>
              @empty
              <h3 class="bg-danger text-white text-center">No hay datos</h3>
              @endforelse

            </tbody>

          </table>
          {{ $bloques->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
