@extends('layouts.master')
@section('title','Alumnos')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h1 class="text-center text-primary">Listado de Alumnos</h1>
          Alumnos |
          Página
          {{$alumnos->currentPage()}}
          de
          {{$alumnos->lastPage()}}
        </div>
        <div class="card-body">
          <h1 class="text-center text-primary"></h1>
          <a class="btn btn-primary my-2" href="{{route('alumnos.create')}}">Agregar Alumno<span
              class="mdi mdi-plus"></span>
          </a>
          <table class="table table-hover table-striped table-bordered table-sm">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre Completo</th>
                <th scope="col">Direccion</th>
                <th scope="col">Telefono</th>
                <th scope="col">Estatus</th>
                <th scope="col">Foto</th>
                <th scope="col" colspan="2" class="text-center">Opciones</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($alumnos as $alu)
              <tr class="text-center">
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$alu->nombre_completo}}</td>
                <td>{{$alu->direccion}}</td>
                <td>{{$alu->telefono}}</td>
                <td>{{($alu->estatus == 1)?'Activo':'Inactivo'}}</td>
                <td><img src='{{ asset("fotosAlumnos/$alu->foto") }}' style="width:50;height:50px" alt=""></td>
                <td>
                  <a class="btn btn-success mdi mdi-pencil-outline"
                    href="{{route('alumnos.edit',$alu->id)}}">Editar</a>
                </td>

              </tr>
              @empty
              <h3 class="bg-danger text-white text-center">No hay datos</h3>
              @endforelse

            </tbody>

          </table>
          {{ $alumnos->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
