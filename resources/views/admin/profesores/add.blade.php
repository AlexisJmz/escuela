@extends('layouts.master')
@section('title','Profesores')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h1 class="text-center text-primary">Agregar Profesor</h1>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ route('profesores.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nombre_completo">Nombre completo:</label>
                <input type="text" class="form-control" name="nombre_completo" id="nombre_completo"
                  value="{{ old('nombre_completo') }}">
              </div>

              <div class="form-group col-md-6">
                <label for="direccion">Dirección:</label>
                <input type="text" class="form-control" name="direccion" id="direccion" value="{{ old('direccion') }}">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="telefono">Telefóno:</label>
                <input type="text" class="form-control" name="telefono" id="telefono" value="{{ old('telefono') }}">
              </div>
              <div class="form-group col-md-6">
                <label for="foto">Foto:</label>
                <input type="file" class="form-control" name="foto" id="foto" value="{{ old('foto') }}">
              </div>

            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="foto">Estatus:</label>
                <select name="estatus" id="estatus" class="form-control">
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Crear Profesor</button>
            <a href="{{ route('profesores.index') }}" class="btn btn-link">Regresar al listado de
              profesores</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
