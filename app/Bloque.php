<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bloque extends Model
{
    protected $table ="bloques";
    protected $fillable=[
      'id','nombre','grado_id'
    ];

    public function grado(){
      return $this->belongsTo(Grado::class,'grado_id');
    }

    public function calificaciones(){
      return $this->hasMany(Calificacion::class);
    }

}
