<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table = "alumnos";
    protected $fillable = ['id','nombre_completo','direccion','telefono','email','foto','estatus'];

    public function calificaciones(){
      return $this->hasMany(Calificacion::class);
    }
}
