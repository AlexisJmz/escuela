<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $table="grados";
    protected $fillable=[
      'id','nombre','profesor_id'
    ];

    public function profesor(){
      return $this->belongsTo(Profesor::class,'profesor_id');
    }

    public function bloques(){
      return $this->hasMany(Bloque::class);
    }
}
