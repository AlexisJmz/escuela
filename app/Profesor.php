<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table= "profesors";
    protected $fillable = ['id','nombre_completo','direccion','telefono','foto','estatus'];

    public function grados(){
      return $this->hasMany(Grado::class);
    }

}
