<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCalificacion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'calificacion'=>'required|numeric',
            'bloque_id'=>'required',
            'alumno_id'=>'required',
        ];
    }

    public function messages(){
      return[
        'calificacion.required'=>'La calificacion es requerido',
        'calificacion.required'=>'La calificacion debe ser numerico',
        'bloque_id.required'=>'El bloque es requerido',
        'alumno_id.required'=>'El alumno es requerido',


      ];
    }
}
