<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestAlumnos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_completo'=>'required',
            'direccion'=>'required',
            'telefono'=>'required|numeric',
            'email'=>'required|email',
            'foto'=>'required|image',

        ];
    }

    public function messages(){
      return[
        'nombre_completo.required'=>'El nombre es requerido',
        'direccion.required'=>'La direccion es requerido',
        'telefono.required'=>'El telefono es requerido',
        'telefono.numeric'=>'El telefono es numerico',
        'email.required'=>'El correo electronico es requerido',
        'email.email'=>'El correo electronico debe ser formato email',
        'foto.required'=>'La foto es requerido',
        'foto.image'=>'La foto debe ser imagen',
      ];
    }
}
