<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestProfesorEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_completo'=>'required',
            'direccion'=>'required',
            'telefono'=>'required',
            'foto'=>'image',

        ];
    }

    public function messages(){
      return[
        'nombre_completo.required'=>'El nombre es requerido',
        'direccion.required'=>'La direccion es requerido',
        'telefono.required'=>'El telefono es requerido',
        'foto.image'=>'La foto debe ser imagen',
      ];
    }
}
