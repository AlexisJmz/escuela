<?php

namespace App\Http\Controllers;

use App\Calificacion;
use App\Alumno;
use App\Bloque;
use Illuminate\Http\Request;
use App\Http\Requests\RequestCalificacion;
use App\Http\Requests\RequestCalificacionEdit;

class CalificacionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calificaciones = Calificacion::paginate(10);
        return view('admin.calificaciones.index',compact('calificaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bloques = Bloque::where('estatus','=','1')->get();
        $alumnos = Alumno::where('estatus','=','1')->get();
        return view('admin.calificaciones.add',compact(['alumnos','bloques']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestCalificacion $request)
    {
        $datosCalificacion = $request->except('_token');
        $a= $datosCalificacion['alumno_id'];
        $b= $datosCalificacion['bloque_id'];
        $result = Calificacion::where('alumno_id','=',$a)->where('bloque_id','=',$b)->count();
        if($result > 0){
          $info="datos ya registrados:  ALUMNO - BLOQUE";
          $typealert="danger";
        }else{
          $typealert="success";
          if(Calificacion::insert($datosCalificacion)){
            $info="Calificacion Guardada exitosamente";
          }else{
            $info="Error al Guardar Calificacion";
            $typealert="danger";
          }


        }
        return back()->with('info',$info)->with('typealert',$typealert);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calificacion  $calificacion
     * @return \Illuminate\Http\Response
     */
    public function show(Calificacion $calificacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calificacion  $calificacion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $alumnos= Alumno::where('estatus','=','1')->get();
      $bloques= Bloque::where('estatus','=','1')->get();
      $calificacion = Calificacion::findOrFail($id);
      return view('admin.calificaciones.edit',compact(['alumnos','bloques','calificacion']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calificacion  $calificacion
     * @return \Illuminate\Http\Response
     */
    public function update(RequestCalificacionEdit $request, $id)
    {
        $datosCalificacion = $request->except(['_token','_method']);

        $typealert="success";
        if(Calificacion::where('id','=',$id)->update($datosCalificacion)){
            $info="Calificacion editada exitosamente";
        }else{
            $info="Error al editar Calificacion";
            $typealert="danger";
        }



        return back()->with('info',$info)->with('typealert',$typealert);
    }


    public function delete()
    {
        $cal = Calificacion::count();
        $typealert="success";
        if($cal > 0){
            Calificacion::truncate();
            $info="Calificaciones eliminadas exitosamente";
        }else{
            $info="Error al eliminar Calificaciones";
            $typealert="danger";
        }
        return redirect()->route('calificaciones.index')->with('info',$info)->with('typealert',$typealert);
    }
}
