<?php

namespace App\Http\Controllers;

use App\Alumno;
use Illuminate\Http\Request;
use App\Http\Requests\RequestAlumnos;
use App\Http\Requests\RequestAlumnosEdit;

class AlumnoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alumnos = Alumno::paginate(10);
        return view('admin.alumnos.index',compact('alumnos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.alumnos.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestAlumnos $request)
    {
        $datosAlumno = $request->except('_token');
        if($request->file('foto') ){
				$foto = $request->file('foto');
				$nombreFoto = time().$foto->getClientOriginalName();
				$datosAlumno['foto']=$nombreFoto;
				$foto->move(public_path().'/fotosAlumnos/',$nombreFoto);
			}
			$typealert="success";
			if(Alumno::insert($datosAlumno)){
				$info="Alumno Guardada exitosamente";
			}else{
				$info="Error al Guardar Alumno";
				$typealert="danger";
			}

			return back()->with('info',$info)->with('typealert',$typealert);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function show(Alumno $alumno)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
      $alumno = Alumno::findOrFail($id);
       return view('admin.alumnos.edit',compact('alumno'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function update(RequestAlumnosEdit $request, $id)
    {
        $datosAlumno = $request->except(['_token','_method']);
        if($request->file('foto') ){
				$foto = $request->file('foto');
				$nombreFoto = time().$foto->getClientOriginalName();
				$datosAlumno['foto']=$nombreFoto;
				$foto->move(public_path().'/fotosAlumnos/',$nombreFoto);
			}
			$typealert="success";
			if(Alumno::where('id','=',$id)->update($datosAlumno)){
				$info="Alumno editado exitosamente";
			}else{
				$info="Error al editado Alumno";
				$typealert="danger";
			}

			return back()->with('info',$info)->with('typealert',$typealert);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alumno $alumno)
    {
        //
    }
}
