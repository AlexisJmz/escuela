<?php

namespace App\Http\Controllers;

use App\Grado;
use App\Profesor;
use Illuminate\Http\Request;
use App\Http\Requests\RequestGrados;

class GradoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $grados = Grado::paginate(10);
      return view('admin.grados.index',compact('grados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profesores = Profesor::where('estatus','=','1')->get();
        return view('admin.grados.add',compact('profesores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestGrados $request)
    {
        $datosGrado = $request->except('_token');
        $typealert="success";
        if(Grado::insert($datosGrado)){
          $info="Grado Guardada exitosamente";
        }else{
          $info="Error al Guardar Grado";
          $typealert="danger";
        }

        return back()->with('info',$info)->with('typealert',$typealert);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grado  $grado
     * @return \Illuminate\Http\Response
     */
    public function show(Grado $grado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grado  $grado
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
      $profesores = Profesor::where('estatus','=','1')->get();
      $grado = Grado::findOrFail($id);
      return view('admin.grados.edit',compact(['grado','profesores']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grado  $grado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $datosGrado = $request->except(['_token','_method']);
			$typealert="success";
			if(Grado::where('id','=',$id)->update($datosGrado)){
					$info="Grado Editada exitosamente";
			}else{
					$info="Grado no fue Editado";
					$typealert="danger";
			}

			return back()->with('info',$info)->with('typealert',$typealert);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grado  $grado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grado $grado)
    {
        //
    }
}
