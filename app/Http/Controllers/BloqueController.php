<?php

namespace App\Http\Controllers;

use App\Bloque;
use Illuminate\Http\Request;
use App\Grado;
use App\Http\Requests\RequestBloques;
use App\Http\Requests\RequestBloquesEdite;
class BloqueController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bloques = Bloque::paginate(10);
        return view('admin.bloques.index',compact('bloques'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grados = Grado::where('estatus','=','1')->get();
        return view('admin.bloques.add',compact('grados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestBloques $request)
    {
        $datosBloque = $request->except('_token');
        $typealert="success";
        if(Bloque::insert($datosBloque)){
          $info="Bloque Guardada exitosamente";
        }else{
          $info="Error al Guardar Bloque";
          $typealert="danger";
        }

        return back()->with('info',$info)->with('typealert',$typealert);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bloque  $bloque
     * @return \Illuminate\Http\Response
     */
    public function show(Bloque $bloque)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bloque  $bloque
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grados= Grado::where('estatus','=','1')->get();
      $bloque = Bloque::findOrFail($id);
      return view('admin.bloques.edit',compact(['bloque','grados']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bloque  $bloque
     * @return \Illuminate\Http\Response
     */
    public function update(RequestBloquesEdite $request,$id)
    {
        $datosBloque = $request->except(['_token','_method']);
        $typealert="success";
        if(Bloque::where('id','=',$id)->update($datosBloque)){
            $info="Bloque Editada exitosamente";
        }else{
            $info="Bloque no fue Editado";
            $typealert="danger";
        }

        return back()->with('info',$info)->with('typealert',$typealert);
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bloque  $bloque
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bloque $bloque)
    {
        //
    }
}
