<?php

namespace App\Http\Controllers;

use App\Profesor;
use Illuminate\Http\Request;
use App\Http\Requests\RequestProfesor;
use App\Http\Requests\RequestProfesorEdit;

class ProfesorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profesores = Profesor::paginate(10);
        return view('admin.profesores.index',compact('profesores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.profesores.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(RequestProfesor $request)
    {
      $datosProfesor = $request->except('_token');
			if($request->file('foto') ){
				$foto = $request->file('foto');
				$nombreFoto = time().$foto->getClientOriginalName();
				$datosProfesor['foto']=$nombreFoto;
				$foto->move(public_path().'/fotosProfesor/',$nombreFoto);
			}
			$typealert="success";
			if(Profesor::insert($datosProfesor)){
				$info="Profesor Guardada exitosamente";
			}else{
				$info="Error al Guardar Profesor";
				$typealert="danger";
			}

			return back()->with('info',$info)->with('typealert',$typealert);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profesor  $profesor
     * @return \Illuminate\Http\Response
     */

    public function edit(Profesor $profesor,$id)
    {
        $profesor = Profesor::findOrFail($id);
				return view('admin.profesores.edit',compact('profesor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profesor  $profesor
     * @return \Illuminate\Http\Response
     */


    public function update(RequestProfesorEdit $request, $id)
    {
        $datosProfesor = $request->except(['_token','_method']);


			if($request->file('foto')){
				$foto = $request->file('foto');
				$nombreFoto = time().$foto->getClientOriginalName();
				$datosProfesor['foto']=$nombreFoto;
				$foto->move(public_path().'/fotosProfesor/',$nombreFoto);
			}
			$typealert="success";
			if(Profesor::where('id','=',$id)->update($datosProfesor)){
					$info="Profesor Editada exitosamente";
			}else{
					$info="Profesor no fue Editado";
					$typealert="danger";
			}

			return back()->with('info',$info)->with('typealert',$typealert);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profesor  $profesor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profesor $profesor,$id)
    {
        //
    }
}
