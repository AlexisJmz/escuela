<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    protected $table = "calificacions";
    protected $fillable = ['id','calificacion','alumno_id','bloque_id'];

    public function alumno(){
      return $this->belongsTo(Alumno::class,'alumno_id');
    }

    public function bloque(){
      return $this->belongsTo(Bloque::class,'bloque_id');
    }
}
